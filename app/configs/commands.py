from flask import Flask
from flask.cli import AppGroup

from app.models.users_model import UserModel
from app.models.credit_cards_model import CreditCardsModel

from app.services import end_session

import click
from faker import Faker


def cli_users(app: Flask):
    cli_group_users = AppGroup('user')
    
    fake = Faker()

    @cli_group_users.command('create')
    @click.argument('quantity')
    def cli_users_create(quantity: int) -> None:
        
        session = app.db.session
        
        for i in range(int(quantity)):
            
            password_to_hash = fake.password()

            user = UserModel(login=fake.name())

            user.password = password_to_hash

            click.echo(f'Login -> {user.login}')
            click.echo(f'Password -> {user.password_hash}')

            end_session(session, user)

    app.cli.add_command(cli_group_users)

    
    @cli_group_users.command('admin')
    def cli_admin_create() -> None:

        session = app.db.session

        password_to_hash = fake.password()

        user = UserModel(login=fake.name(), is_admin=True)

        user.password = password_to_hash

        click.echo(f'Login -> {user.login}')
        click.echo(f'Password -> {user.password_hash}')

        end_session(session, user)

    app.cli.add_command(cli_group_users)


    @cli_group_users.command('users_credit_cards')
    @click.argument('quantity')
    def cli_users_create(quantity: int) -> None:
        
        session = app.db.session
        
        for i in range(int(quantity)):
            
            password_to_hash = fake.password()

            user = UserModel(login=fake.name())
            
            user.password = password_to_hash
            
            session.add(user)

            session.commit()

            credit_card = CreditCardsModel(
                expire_date = fake.credit_card_expire(start='now', end='+10y', date_format='%m/%y'),
                number = fake.credit_card_number(card_type=None),
                provider = fake.credit_card_provider(card_type=None),
                security_code = fake.credit_card_security_code(card_type=None)[:3],
                user_id = user.id 
            )

            click.echo(f'Login -> {user.login}')
            click.echo(f'Password -> {user.password_hash}')

            session.add(credit_card)
            
            session.commit()

    app.cli.add_command(cli_group_users)



def init_app(app: Flask):
    cli_users(app)





    

