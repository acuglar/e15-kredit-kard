from app.configs.database import db
from sqlalchemy import Integer, String, Column, ForeignKey 


class CreditCardsModel(db.Model):
    __tablename__ = 'credit_cards'

    id = Column(Integer, primary_key=True)

    expire_date = Column(String(), nullable=False) 
    number = Column(String(), nullable=False, unique=True)
    provider = Column(String(50), nullable=False)
    security_code = Column(String(3), nullable=False)

    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)

    user = db.relationship('UserModel', backref='credit_cards')
